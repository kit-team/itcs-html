﻿using ITCSCloud.Common;
using ITCSCloud.Extensions;
using Microsoft.AspNetCore.Http;

namespace ITCSCloud.Services
{
    public class LocalizationContext : ILocalizationContext
    {
        public LocalizationContext(IHttpContextAccessor httpContextAccessor, ILanguageCodeMap languageCodeMap)
        {
            RequestLanguageCode = httpContextAccessor.HttpContext.GetRequestCulture()
                .Culture
                .Name;

            RequestCultureId = languageCodeMap[RequestLanguageCode];
        }

        public byte RequestCultureId { get; }

        public string RequestLanguageCode { get; }
    }
}