﻿using ITCSCloud.ViewModels;

namespace ITCSCloud.Services
{
    public class MenuProvider
    {
        public MenuProvider()
        {
            // TODO load those from database
            MainMenuItems = new[]
            {
                new MenuItemViewModel
                {
                    Title = "Applicants",
                    PagePath = "/Applicants/Index",
                    Children =
                    {
                        new MenuItemViewModel
                        {
                            Title = "SubItem1",
                            PagePath = ""
                        },

                        new MenuItemViewModel
                        {
                            Title = "SubItem2",
                            PagePath = ""
                        },

                        new MenuItemViewModel
                        {
                            Title = "SubItem3",
                            PagePath = ""
                        }
                    }
                },

                new MenuItemViewModel
                {
                    Title = "Students",
                    PagePath = "/Students/Index",
                    Children =
                    {
                        new MenuItemViewModel
                        {
                            Title = "SubItem1",
                            PagePath = ""
                        },

                        new MenuItemViewModel
                        {
                            Title = "SubItem2",
                            PagePath = ""
                        },

                        new MenuItemViewModel
                        {
                            Title = "SubItem3",
                            PagePath = ""
                        },
                    }
                },

                new MenuItemViewModel
                {
                    Title = "Alumni",
                    PagePath = "/Alumni/Index",
                    Children =
                    {
                        new MenuItemViewModel
                        {
                            Title = "SubItem1",
                            PagePath = ""
                        },

                        new MenuItemViewModel
                        {
                            Title = "SubItem2",
                            PagePath = ""
                        },

                        new MenuItemViewModel
                        {
                            Title = "SubItem3",
                            PagePath = ""
                        }
                    }
                },

                new MenuItemViewModel
                {
                    Title = "News",
                    PagePath = "/News/Index",
                    Children =
                    {
                        new MenuItemViewModel
                        {
                            Title = "SubItem1",
                            PagePath = ""
                        },

                        new MenuItemViewModel
                        {
                            Title = "SubItem2",
                            PagePath = ""
                        },

                        new MenuItemViewModel
                        {
                            Title = "SubItem3",
                            PagePath = ""
                        }
                    }
                },

                new MenuItemViewModel
                {
                    Title = "About",
                    PagePath = "/About/Index",
                    Children =
                    {
                        new MenuItemViewModel
                        {
                            Title = "Academic",
                            PagePath = "/About/Academic"
                        },

                        new MenuItemViewModel
                        {
                            Title = "SubItem2",
                            PagePath = ""
                        }
                    }
                },
            };
        }

        public MenuItemViewModel[] MainMenuItems { get; }
    }
}