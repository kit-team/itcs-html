﻿using System.IO;
using ITCSCloud.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace ITCSCloud.Services
{
    /// <summary>
    /// This class is used by Entity Framework Core to perform migrations
    /// </summary>
    public class DesignTimeDbContextProvider : IDesignTimeDbContextFactory<DepartmentDbContext>
    {
        public DepartmentDbContext CreateDbContext(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var builder = new DbContextOptionsBuilder<DepartmentDbContext>()
                .UseSqlServer(configuration.GetConnectionString("DefaultConnection"),
                    a => a.MigrationsAssembly("ITCSCloud"));

            return new DepartmentDbContext(builder.Options);
        }
    }
}