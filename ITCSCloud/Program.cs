﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace ITCSCloud
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHost(args).Run();
        }

        // Renamed from BuildWebHost becaus of EF migrations bugs
        private static IWebHost CreateWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
