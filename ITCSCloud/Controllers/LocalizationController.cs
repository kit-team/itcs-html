﻿using System;
using ITCSCloud.ViewComponents.LanguageSelector;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;

namespace ITCSCloud.Controllers
{
    [Route("[controller]")]
    public class LocalizationController : Controller
    {
        [HttpPost("[action]/{languageCode}")]
        public IActionResult SetLanguage(string languageCode, LanguageSelectorViewModel languageSelector)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(languageCode)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return LocalRedirect(languageSelector.ReturnUrl ?? "/");
        }
    }
}