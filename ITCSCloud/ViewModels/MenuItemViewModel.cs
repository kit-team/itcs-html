﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ITCSCloud.ViewModels
{
    public class MenuItemViewModel
    {
        public MenuItemViewModel Parent { get; }

        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the font-awesome class name for this <see cref="MenuItemViewModel"/>.
        /// </summary>
        public string IconClassName { get; set; }

        public string PagePath { get; set; }

        public bool IsVisible { get; set; } = true;

        public bool IsEnabled { get; set; } = true;

        public bool HasPath(string path)
        {
            return path.Equals(PagePath, StringComparison.OrdinalIgnoreCase)
                   || Children.Any(x => x.HasPath(path));
        }

        public IList<MenuItemViewModel> Children { get; } 
            = new List<MenuItemViewModel>();
    }
}