﻿namespace ITCSCloud.ViewModels
{
    public class MainMenuViewModel
    {
        public string CurrentPagePath { get; set; }

        public MenuItemViewModel[] Items { get; set; }
    }
}