$( document ).ready(function() {

    $(function() {
        var e = window.innerWidth, 
        body = $("body"), 
        burger = $(".burger"), 
        container = $('<div class="navwindow"></div>');
        li = $(".nav ul li");

        burger.click(function(e) {
            e.preventDefault(),
            showContainer();
        });
        container.click(function(e) {
            body.toggleClass("__navactive"),
            container.hide()
        });
        showContainer = function() {
            container.toggle(),
            body.toggleClass("__navactive")
        };

    })
});