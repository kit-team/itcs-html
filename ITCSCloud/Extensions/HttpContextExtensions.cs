﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;

namespace ITCSCloud.Extensions
{
    public static class HttpContextExtensions
    {
        public static RequestCulture GetRequestCulture(this HttpContext context)
            => context.Features.Get<IRequestCultureFeature>().RequestCulture;
    }
}