﻿using System.Globalization;
using System.Linq;
using ITCSCloud.Common;
using ITCSCloud.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace ITCSCloud.Extensions
{
    public static class LocalizationWithResourcesFolderExtensions
    {
        public static IServiceCollection AddLocalizationWithResourcesFolder(this IServiceCollection services)
        {
            services.AddSingleton<ILanguageCodeMap, LanguageCodeToCultureIdMap>();
            services.AddScoped<ILocalizationContext, LocalizationContext>();

            services.AddLocalization(x => { x.ResourcesPath = "Resources"; });
            services.AddSingleton<IConfigureOptions<RequestLocalizationOptions>, ConfigureRequestLocalizationOptions>();

            return services;
        }
    }

    public class ConfigureRequestLocalizationOptions : IConfigureOptions<RequestLocalizationOptions>
    {
        private readonly string[] _languageCodes;

        public ConfigureRequestLocalizationOptions(ILanguageCodeMap languageCodeMap)
        {
            _languageCodes = languageCodeMap.Keys
                .ToArray();
        }

        public void Configure(RequestLocalizationOptions options)
        {
            var supportedCultures = _languageCodes
                        .Select(x => new CultureInfo(x))
                        .ToList();

            //Will use the same cultures for ui and general
           options.SupportedCultures = options.SupportedUICultures = supportedCultures;
        }
    }
}