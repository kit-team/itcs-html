﻿using System.Collections.Generic;
using ITCSCloud.Common;
using ITCSCloud.Data;
using Microsoft.Extensions.DependencyInjection;

namespace ITCSCloud.Extensions
{
    public class LanguageCodeToCultureIdMap : Dictionary<string, byte>, ILanguageCodeMap
    {
        public LanguageCodeToCultureIdMap(IServiceScopeFactory serviceScopeFactory)
        {
            using (var scope = serviceScopeFactory.CreateScope())
            {
                using (var context = scope.ServiceProvider.GetRequiredService<DepartmentDbContext>())
                {
                    foreach (var culture in context.SupportedCultures)
                    {
                        Add(culture.LanguageCode, culture.Id);
                    }
                }
            }
        }
    }
}