﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace ITCSCloud.TagHelpers
{
    [HtmlTargetElement(Attributes = TagHelperPrefix + "*")]
    public class ConditionalClassTagHelper : TagHelper
    {
        private const string TagHelperPrefix = "conditional-class-";

        [HtmlAttributeName("", DictionaryAttributePrefix = TagHelperPrefix)]
        public IDictionary<string, bool> ConditionalClasses { get; set; }
            = new Dictionary<string, bool>(StringComparer.OrdinalIgnoreCase);

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var activeClassesString = string.Join(" ",
                ConditionalClasses
                    .Where(x => x.Value)
                    .Select(x => x.Key));

            if (!string.IsNullOrWhiteSpace(activeClassesString))
            {
                output.Attributes.SetAttribute("class",
                    output.Attributes.TryGetAttribute("class", out var attribute)
                        ? $"{attribute.Value} {activeClassesString}"
                        : activeClassesString);
            }
        }
    }
}