﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ITCSCloud.ViewComponents
{
    [ViewComponent(Name = nameof(Header))]
    public class Header : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View();
        }
    }
}