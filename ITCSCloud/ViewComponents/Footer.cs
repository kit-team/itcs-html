﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ITCSCloud.ViewComponents
{
    [ViewComponent(Name = nameof(Footer))]
    public class Footer : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View();
        }
    }
}