﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ITCSCloud.ViewComponents.LanguageSelector
{
    public class LanguageSelectorViewModel
    {
        [BindNever]
        public IList<SelectListItem> SupportedCultures { get; set; }

        [HiddenInput]
        public string ReturnUrl { get; set; }
    }
}