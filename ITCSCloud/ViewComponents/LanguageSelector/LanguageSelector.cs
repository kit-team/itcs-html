﻿using System.Linq;
using System.Threading.Tasks;
using ITCSCloud.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Options;

namespace ITCSCloud.ViewComponents.LanguageSelector
{
    [ViewComponent(Name = nameof(LanguageSelector))]
    public class LanguageSelector : ViewComponent
    {
        private readonly RequestLocalizationOptions _locOptions;

        public LanguageSelector(IOptions<RequestLocalizationOptions> LocOptions)
        {
            _locOptions = LocOptions.Value;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var requestCultureName = HttpContext.Features.Get<IRequestCultureFeature>()
                .RequestCulture.UICulture.Name;

            var cultureItems = _locOptions.SupportedUICultures
                .Select(c => new SelectListItem
                {
                    Value = c.Name,
                    Text = c.DisplayName,
                    Selected = c.Name == requestCultureName
                })
                .ToList();

            return View(new LanguageSelectorViewModel
            {
                SupportedCultures = cultureItems,
                ReturnUrl = HttpContext.Request.GetEncodedPathAndQuery()
            });
        }
    }
}