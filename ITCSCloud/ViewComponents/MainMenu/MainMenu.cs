﻿﻿using System;
using System.Threading.Tasks;
using ITCSCloud.Services;
using ITCSCloud.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace ITCSCloud.ViewComponents.MainMenu
{
    [ViewComponent(Name = "MainMenu")]
    public class MainMenu : ViewComponent
    {
        private readonly MenuProvider _menuProvider;

        public MainMenu(MenuProvider menuProvider)
        {
            _menuProvider = menuProvider;
        }

        public async Task<IViewComponentResult> InvokeAsync(MenuDisplayContext displayFor)
        {
            if (RouteData.Values.TryGetValue("page", out var pageValue)
                && pageValue is string pagePath)
            {
                var menu = new MainMenuViewModel
                {
                    CurrentPagePath = pagePath,
                    Items = _menuProvider.MainMenuItems
                };

                switch (displayFor)
                {
                    case MenuDisplayContext.Header:
                        return View("HeaderMenu", menu);

                    case MenuDisplayContext.Footer:
                        return View("FooterMenu", menu);

                    case MenuDisplayContext.SidePanel:
                        return View("SidePanel", menu);

                    default:
                        throw new ArgumentOutOfRangeException(nameof(displayFor));
                }
            }

            throw new NotSupportedException("Menu is compatible only with Razor Pages routing");
        }
    }
}