﻿﻿namespace ITCSCloud.ViewComponents.MainMenu
{
    public enum MenuDisplayContext
    {
        Header,
        Footer,
        SidePanel
    }
}