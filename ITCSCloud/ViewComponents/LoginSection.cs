﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ITCSCloud.ViewComponents
{
    [ViewComponent(Name = nameof(LoginSection))]
    public class LoginSection : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync(bool isAuthenticated)
        {
            return isAuthenticated ? View("Authenticated") : View("NotAuthenticated");
        }
    }
}