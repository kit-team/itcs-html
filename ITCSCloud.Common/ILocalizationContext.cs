﻿namespace ITCSCloud.Common
{
    public interface ILocalizationContext
    {
        byte RequestCultureId { get; }

        string RequestLanguageCode { get; }
    }
}