﻿using System.Collections.Generic;

namespace ITCSCloud.Common
{
    public interface ILanguageCodeMap : IReadOnlyDictionary<string, byte>
    {
    }
}