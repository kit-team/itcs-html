﻿using System;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using ITCSCloud.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ITCSCloud.Data
{
    public class DepartmentDbContext : IdentityDbContext<ApplicationUser>
    {
        public DepartmentDbContext(DbContextOptions<DepartmentDbContext> options)
        : base(options)
        {
        }

        public DbSet<Article> Articles { get; set; }

        public DbSet<LocalizedArticle> LocalizedArticles { get; set; }

        public DbSet<Alumni> Alumnis { get; set; }

        public DbSet<LocalizedAlumni> LocalizedAlumnis { get; set; }

        public DbSet<Academic> Academics { get; set; }

        public DbSet<LocalizedAcademic>  LocalizedAcademics { get; set; }

        public DbSet<Discipline> Disciplines { get; set; }

        public DbSet<LocalizedDiscipline> LocalizedDisciplines { get; set; }

        public DbSet<AssignedDiscipline> AssignedDisciplines { get; set; }

        public DbSet<ScheduleItem> ScheduleItems { get; set; }

        public DbSet<LocalizedScheduleItem> LocalizedScheduleItems { get; set; }

        public DbSet<ScheduleGroup> ScheduleGroups { get; set; }

        public DbSet<LocalizedScheduleGroup> LocalizedScheduleGroups { get; set; }

        public DbSet<Culture> SupportedCultures { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Culture>().ToTable("Culture");

            builder
                .ConfigureLocalizedEntity<Academic, LocalizedAcademic>(x => x.AcademicId, x => x.Academic)
                .ConfigureLocalizedEntity<Alumni, LocalizedAlumni>(x => x.AlumniId, x => x.Alumni)
                .ConfigureLocalizedEntity<Article, LocalizedArticle>(x => x.ArticleId, x => x.Article)
                .ConfigureLocalizedEntity<Discipline, LocalizedDiscipline>(x => x.DisciplineId, x => x.Discipline)
                .ConfigureLocalizedEntity<Post, LocalizedPost>(x => x.PostId, x => x.Post)
                .ConfigureLocalizedEntity<ScheduleGroup, LocalizedScheduleGroup>(x => x.ScheduleGroupId, x => x.ScheduleGroup)
                .ConfigureLocalizedEntity<ScheduleItem, LocalizedScheduleItem>(x => x.ScheduleItemId, x => x.ScheduleItem);

            builder.Entity<AssignedDiscipline>()
                .HasKey(x => new
                {
                    x.AcademicId,
                    x.DisciplineId
                });

            builder.Entity<ScheduleItem>()
                .HasIndex(x => new
                {
                    x.Timing,
                    x.OrganizerId
                })
                .IsUnique();

            builder.Entity<ScheduleGroup>()
                .HasIndex(x => x.HexARGBColor)
                .IsUnique();

            builder.Entity<Post>()
                .HasOne(x => x.Author)
                .WithMany(x => x.Posts)
                .HasForeignKey(x => x.AuthorId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<AssignedDiscipline>()
                .ToTable(nameof(AssignedDiscipline));

            builder.Entity<AssignedDiscipline>()
                .HasOne(x => x.Discipline)
                .WithMany(x => x.Assignations)
                .HasForeignKey(x => x.DisciplineId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<AssignedDiscipline>()
                .HasOne(x => x.Academic)
                .WithMany(x => x.AssignedDisciplines)
                .HasForeignKey(x => x.AcademicId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}