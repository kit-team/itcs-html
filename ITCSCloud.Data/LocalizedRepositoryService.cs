﻿using System.Linq;
using ITCSCloud.Common;
using ITCSCloud.Entities;
using Microsoft.EntityFrameworkCore;

namespace ITCSCloud.Data
{
    public abstract class LocalizedRepositoryService<TLocalizationEntity>
        where TLocalizationEntity: LocalizedEntity
    {
        private readonly DepartmentDbContext _dbContext;
        private readonly ILocalizationContext _localizationContext;

        protected LocalizedRepositoryService(DepartmentDbContext dbContext, ILocalizationContext localizationContext)
        {
            _dbContext = dbContext;
            _localizationContext = localizationContext;
        }

        protected IQueryable<TLocalizationEntity> GetLocalizedItems(int pageSize, int pageNumber)
        {
            var query = from localizedEntity in _dbContext.Set<TLocalizationEntity>()
                where localizedEntity.CultureId == _localizationContext.RequestCultureId
                select localizedEntity;

            return query.AsNoTracking();
        }
    }
}