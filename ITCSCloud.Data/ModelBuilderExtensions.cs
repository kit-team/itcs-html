﻿using System;
using System.Linq.Expressions;
using ITCSCloud.Entities;
using Microsoft.EntityFrameworkCore;

namespace ITCSCloud.Data
{
    public static class ModelBuilderExtensions
    {
        public static ModelBuilder ConfigureLocalizedEntity<TLocalizable, TLocalization>(this ModelBuilder builder,
            Expression<Func<TLocalization, int>> foreignKeyExpression, Expression<Func<TLocalization, TLocalizable>> navigationExpression)
            where TLocalizable : LocalizableEntity<TLocalization>, new()
            where TLocalization : LocalizedEntity, new()
        {
            if (foreignKeyExpression == null)
            {
                throw new ArgumentNullException(nameof(foreignKeyExpression));
            }

            if (navigationExpression == null)
            {
                throw new ArgumentNullException(nameof(navigationExpression));
            }

            var foreignKeyPropertyName = GetPropertyNameFromExpression(foreignKeyExpression);
            var navigationPropertyName = GetPropertyNameFromExpression(navigationExpression);

            builder.Entity<TLocalizable>()
                .ToTable(typeof(TLocalizable).Name)
                .HasMany(x => x.Localizations)
                .WithOne(navigationPropertyName)
                .HasForeignKey(foreignKeyPropertyName)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<TLocalization>()
                .ToTable(typeof(TLocalization).Name)
                .HasKey(nameof(LocalizedEntity.CultureId), foreignKeyPropertyName);

            return builder;
        }

        private static string GetPropertyNameFromExpression(LambdaExpression expression)
        {
            try
            {
                var memberExpression = (MemberExpression)expression.Body;
                return memberExpression.Member.Name;
            }
            catch (InvalidCastException)
            {
                throw new ArgumentException(nameof(expression));
            }
        }
    }

    
}