﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ITCSCloud.Entities
{
    public class Discipline : LocalizableEntity<LocalizedDiscipline>
    {
        [Key]
        public int Id { get; set; }

        public bool IsDeprecated { get; set; }

        public ICollection<AssignedDiscipline> Assignations { get; set; }
    }
}