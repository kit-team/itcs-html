﻿using System.ComponentModel.DataAnnotations;

namespace ITCSCloud.Entities
{
    public class LocalizedScheduleGroup : LocalizedEntity
    {
        public int ScheduleGroupId { get; set; }

        public ScheduleGroup ScheduleGroup { get; set; }

        [Required]
        [MaxLength(75)]
        public string Title { get; set; }

        [MaxLength(300)]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
    }
}