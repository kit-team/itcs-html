﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ITCSCloud.Entities
{
    public class Post : LocalizableEntity<LocalizedPost>
    {
        [Key]
        public int Id { get; set; }

        public string AuthorId { get; set; }

        public ApplicationUser Author { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime PublishDateTime { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? UpdatedDateTime { get; set; }
    }
}