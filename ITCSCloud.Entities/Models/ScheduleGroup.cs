﻿using System.ComponentModel.DataAnnotations;

namespace ITCSCloud.Entities
{
    public class ScheduleGroup : LocalizableEntity<LocalizedScheduleGroup>
    {
        [Key]
        public int Id { get; set; }

        public bool IsDisabled { get; set; }

        [Required]
        public string HexARGBColor { get; set; }
    }
}