﻿using System.ComponentModel.DataAnnotations;

namespace ITCSCloud.Entities
{
    public class LocalizedAcademic : LocalizedEntity
    {
        public int AcademicId { get; set; }

        public Academic Academic { get; set; }

        [Required]
        [MaxLength(50)]
        public string FullName { get; set; }

        [Required]
        [MaxLength(75)]
        public string Position { get; set; }

        [Required]
        [MaxLength(300)]
        [DataType(DataType.MultilineText)]
        public string Overview { get; set; }

        [MaxLength(50)]
        public string AcademicDegree { get; set; }

        [MaxLength(150)]
        public string AcademicTitle { get; set; }

        [Required]
        [MaxLength(5000)]
        [DataType(DataType.Html)]
        public string Details { get; set; }
    }
}