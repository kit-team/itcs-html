﻿using System.ComponentModel.DataAnnotations;

namespace ITCSCloud.Entities
{
    public class LocalizedDiscipline : LocalizedEntity
    {
        public int DisciplineId { get; set; }

        public Discipline Discipline { get; set; }

        [Required]
        [MaxLength(50)]
        public string Title { get; set; }

        [Required]
        [DataType(DataType.Html)]
        public string Description { get; set; }
    }
}