﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace ITCSCloud.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public Academic Academic { get; set; }

        public ICollection<Post> Posts { get; set; }
    }
}