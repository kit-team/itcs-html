﻿namespace ITCSCloud.Entities
{
    public enum AssignationType
    {
        Lecturer,
        Assistant
    }
}