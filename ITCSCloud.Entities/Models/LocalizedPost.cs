﻿using System.ComponentModel.DataAnnotations;

namespace ITCSCloud.Entities
{
    public class LocalizedPost : LocalizedEntity
    {
        public int PostId { get; set; }

        public Post Post { get; set; }

        [Required]
        [DataType(DataType.Html)]
        public string Content { get; set; }
    }
}