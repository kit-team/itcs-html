﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ITCSCloud.Entities
{
    public class Article : LocalizableEntity<LocalizedArticle>
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime PublishDateTime { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? UpdatedDateTime { get; set; }

    }
}