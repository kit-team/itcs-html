﻿using System.ComponentModel.DataAnnotations;

namespace ITCSCloud.Entities
{
    public class Alumni : LocalizableEntity<LocalizedAlumni>
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [DataType(DataType.ImageUrl)]
        public string PhotoPath { get; set; }
    }
}