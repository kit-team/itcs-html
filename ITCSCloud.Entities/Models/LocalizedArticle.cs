﻿using System.ComponentModel.DataAnnotations;

namespace ITCSCloud.Entities
{
    public class LocalizedArticle : LocalizedEntity
    {
        public int ArticleId { get; set; }

        public Article Article { get; set; }

        [Required]
        [MaxLength(75)]
        public string Title { get; set; }

        [Required]
        [DataType(DataType.ImageUrl)]
        public string PreviewPhotoPath { get; set; }

        [Required]
        [DataType(DataType.Html)]
        public string PreviewContent { get; set; }

        [Required]
        [DataType(DataType.Html)]
        public string Content { get; set; }
    }
}