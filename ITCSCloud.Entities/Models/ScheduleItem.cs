﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ITCSCloud.Entities
{
    public class ScheduleItem : LocalizableEntity<LocalizedScheduleItem>
    {
        [Key]
        public int Id { get; set; }

        public int? DisciplineId { get; set; }

        [ForeignKey(nameof(DisciplineId))]
        public Discipline Discipline { get; set; }

        public int OrganizerId { get; set; }

        [ForeignKey(nameof(OrganizerId))]
        public Academic Organizer { get; set; }

        public int? GroupId { get; set; }

        [ForeignKey(nameof(GroupId))]
        public ScheduleGroup Group { get; set; }

        [Required]
        public DateTime Timing { get; set; }
    }
}