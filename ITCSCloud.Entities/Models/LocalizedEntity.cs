﻿namespace ITCSCloud.Entities
{
    public abstract class LocalizedEntity
    {
        public byte CultureId { get; set; }

        public Culture Culture { get; set; }
    }
}