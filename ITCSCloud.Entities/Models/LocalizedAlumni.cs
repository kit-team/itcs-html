﻿using System.ComponentModel.DataAnnotations;

namespace ITCSCloud.Entities
{
    public class LocalizedAlumni : LocalizedEntity
    {
        public int AlumniId { get; set; }

        public Alumni Alumni { get; set; }

        [Required]
        [MaxLength(50)]
        public string FullName { get; set; }

        [Required]
        [MaxLength(75)]
        public string CurrentPosition { get; set; }

        [Required]
        [MaxLength(400)]
        [DataType(DataType.Html)]
        public string Acknowledgment { get; set; }
    }
}