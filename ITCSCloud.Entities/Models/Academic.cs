﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ITCSCloud.Entities
{
    public class Academic : LocalizableEntity<LocalizedAcademic>
    {
        [Key]
        public int Id { get; set; }

        public string ApplicationUserId { get; set; }

        [ForeignKey(nameof(ApplicationUserId))]
        public ApplicationUser User { get; set; }

        [DataType(DataType.ImageUrl)]
        public string PhotoPath { get; set; }

        public bool IsFired { get; set; }

        public ICollection<AssignedDiscipline> AssignedDisciplines { get; set; }

        public ICollection<ScheduleItem> Schedule { get; set; }
    }
}