﻿using System.ComponentModel.DataAnnotations;

namespace ITCSCloud.Entities
{
    public class LocalizedScheduleItem : LocalizedEntity
    {
        public int ScheduleItemId { get; set; }

        public ScheduleItem ScheduleItem { get; set; }

        [MaxLength(100)]
        public string TitleOverride { get; set; }
    }
}