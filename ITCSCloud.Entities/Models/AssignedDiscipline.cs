﻿using System.ComponentModel.DataAnnotations;

namespace ITCSCloud.Entities
{
    public class AssignedDiscipline
    {
        public int AcademicId { get; set; }

        public Academic Academic { get; set; }

        public int DisciplineId { get; set; }

        public Discipline Discipline { get; set; }

        [Required]
        public AssignationType AssignationType { get; set; }
    }
}