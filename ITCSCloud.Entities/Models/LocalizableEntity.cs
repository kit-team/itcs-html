﻿using System.Collections.Generic;

namespace ITCSCloud.Entities
{
    public abstract class LocalizableEntity<TLocalization> where TLocalization: LocalizedEntity
    {
        public ICollection<TLocalization> Localizations { get; set; }
    }
}